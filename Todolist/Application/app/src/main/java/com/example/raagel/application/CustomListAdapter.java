package com.example.raagel.application;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.logging.Filter;

/**
 * Created by Raagel on 25.04.2016.
 */
public class CustomListAdapter extends BaseAdapter{

    private ArrayList<Task> tasks;
    private LayoutInflater inflater;

    public CustomListAdapter(MainActivity activity, ArrayList<Task> tasks){
        this.tasks = tasks;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Object getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.list_item, parent, false);

        Task task = tasks.get(position);

        TextView textViewDescription = (TextView) convertView.findViewById(R.id.list_text);
        textViewDescription.setText(task.getDescription());

        TextView textViewDate = (TextView) convertView.findViewById(R.id.list_item_date);
        textViewDate.setText(task.getCreatedDate().toString());

        return convertView;
    }
    /*@Override
    public Filter getFilter(){
        Filter filter = new Filter() {
        }
    }
    @Override
    public FilterResults preformFiltering(Charset){
        FilterResults results = new FilterResults();
        results values = ;
        results count = ;
        return results;
    }
    public void publishResults(CharSequence.s,FilterResults);
    }
    */

}



package com.example.raagel.application;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonAdd;
    Button buttonDelete;

    private EditText searchInput;
    private CustomListAdapter listAdapter;
    private ArrayList <Task> tasks;

    private ListView listView;
    //private ArrayAdapter <String> listAdapter;
    private CustomComparator comparator;

    private MenuItem menuItemName;
    private MenuItem menuItemAscending;

    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        //searchInput = (EditText)findViewById(R.id.textView);
        //searchInput.addTextChangedListener(new TextWatcher());


        listView = (ListView) findViewById(R.id.my_list);




        tasks = new ArrayList<Task>();
        tasks.add(new Task("First"));
        tasks.add(new Task("Second"));
        tasks.add(new Task("Third"));
        tasks.add(new Task("Fourth"));
        tasks.add(new Task("Fifth"));

        comparator = new CustomComparator();
        comparator.setFilter(true, false);

        listAdapter = new CustomListAdapter(this, tasks);
        //listAdapter = new ArrayAdapter <String> (this, R.layout.list_item, R.id.list_text, tasks);
        listView.setAdapter(listAdapter);

        buttonAdd=(Button)findViewById(R.id.button);
        buttonDelete=(Button)findViewById(R.id.button2);
        buttonAdd.setOnClickListener(this);
        buttonDelete.setOnClickListener(this);
        new HTTPUtils().execute("Dummy");
    }


    @Override
    public void onClick(View v) {
        if (v == buttonAdd) {
            tasks.add(new Task("New Task"));
            listAdapter.notifyDataSetChanged();
        }
        if (v == buttonDelete) {
            tasks.remove(tasks.get(0));
            listAdapter.notifyDataSetChanged();
        }
    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        menuItemName = menu.findItem(R.id.action_name);
        menuItemAscending = menu.findItem(R.id.action_ascending);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_name:
                updateFilter(item);
                return true;
            case R.id.action_ascending:
                updateFilter(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateFilter(MenuItem item ) {
        item.setChecked(!item.isChecked());
        comparator.setFilter(menuItemName.isChecked(), menuItemAscending.isChecked());
        Collections.sort(tasks, comparator);
        listAdapter.notifyDataSetChanged();
    }
    /*
    public void beforeChanged(){

    }
    public void onTextChanged(CharSequence s, ){
        adapter.getFilter().filter(s);
    }
    public void afterChanged(){

    }*/

}

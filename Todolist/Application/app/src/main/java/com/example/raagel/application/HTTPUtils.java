package com.example.raagel.application;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Raagel on 29.04.2016.
 */
public class HTTPUtils extends AsyncTask <String,String,String> {
    private HttpURLConnection urlConnection = null;

    @Override
    public String doInBackground(String ... params) {
        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL("http://192.168.1.39:3000/db");
            urlConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            return result.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }
    @Override
    public void onPostExecute(String result){
        Log.d("Test", result);
        JSONObject test = null;
        try {
            test = new JSONObject(result);
            Log.d("TEST", test.toString());
            Gson gson = new GsonBuilder().create();
            Task task = gson.fromJson(result.toString(),Task.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

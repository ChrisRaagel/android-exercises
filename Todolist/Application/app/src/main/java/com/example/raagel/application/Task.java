package com.example.raagel.application;

import java.util.Date;

/**
 * Created by Raagel on 25.04.2016.
 */
public class Task {

    private int id;
    private String description;
    private Date createdDate;
    private boolean completed;
    private Date modifiedDate;

    public Task(String description) {
        this.description = description;
        createdDate = new Date();
        modifiedDate = createdDate;
    }

    public String getDescription() {
        return description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }
}

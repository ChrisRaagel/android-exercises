package com.example.raagel.application;

import java.util.Comparator;

/**
 * Created by Raagel on 18.04.2016.
 */
public class CustomComparator implements Comparator<Task> {

    private boolean sortByName;
    private boolean sortAscending;

    public void setFilter(boolean sortByName, boolean sortAscending) {
        this.sortByName = sortByName;
        this.sortAscending = sortAscending;
    }

    @Override
    public int compare(Task left, Task right) {

        String lhs = left.getDescription();
        String rhs = right.getDescription();

        if (sortByName) {
            if (sortAscending) {
                return lhs.compareTo(rhs);
            } else {
                return rhs.compareTo(lhs);
            }
        } else {
            int result = 0;
            if (lhs.length() < rhs.length()) {
                result = -1;
            } else if (lhs.length() > rhs.length()) {
                result = 1;
            }
            return (sortAscending ? -result : result);

        }
    }

    public boolean isSortAscending() {
        return sortAscending;
    }

    public boolean isSortByName() {
        return sortByName;
    }
}